var Error           = require('../generic/Error');
var StrictArguments = require("./StrictArguments");

var decorator       = function(target) {
    if (this instanceof decorator) throw new Error(decorator.ERROR_DECORATOR_NOT_INSTANTIABLE, 'The Model cant\' be instantiated');
    //
    var _connector = null;
    Object.defineProperty(target, 'setDriver', { enumerable: true, value: function (v)                                  {
        _connector = v;
    }});
    Object.defineProperty(target, 'getDriver', { enumerable: true, value: function ()                                   {
        return _connector;
    }});
    //
    return target;
};

module.exports = StrictArguments(decorator);
