should      = (require 'chai').should()
#
describe 'The Abstract Schema', ->
    Schema   = require "../../src/abstract/Schema"    
    it 'Should be defined',                     -> Schema.should.exist
    #
    describe "As an instance", ->
        instance            = Schema {}
        it 'should implement the "setDriver"',  -> instance.should.respondTo 'setDriver'
        it 'should implement the "getDriver"',  -> instance.should.respondTo 'getDriver'
